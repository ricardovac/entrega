package br.com.solutis;

import robocode.AdvancedRobot;
import robocode.ScannedRobotEvent;
import robocode.WinEvent;

import java.awt.*;

public class Acima extends AdvancedRobot {
    int gunDirection = 1;
    public void run() {
        setColors(Color.blue,Color.red,Color.red,Color.magenta,Color.magenta);
        while(true){
            turnGunRight(360);
        }
    }
    public void onScannedRobot(ScannedRobotEvent e){
        setTurnRight(e.getBearing());
        setFire(3);
        setAhead(100);
        gunDirection = -gunDirection;
        setTurnGunRight(360 * gunDirection);
        execute();
    }
    public void onWin(WinEvent e) {
        for (int i = 0; i < 50; i++) {
            turnRight(30);
            turnLeft(30);
        }
    }
}

