# Parabéns

<p align="center">
    <img src="https://i.giphy.com/media/S3nZ8V9uemShxiWX8g/giphy.webp" alt="MarineGEO circle logo" style="height: 100px; width:100px;"/>
</p>


## Você encontrou o repositório
## Siga as instruções abaixo para realizar a entrega.

\
&nbsp;

### [0] - Por favor. Tenha certeza que o seu código está funcionando antes de começar o processo.

### [1] - Fork o repositório - [manual](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html)

### [2] - Crie um diretório com o nome da sua tribo no caminho `/robots/` para armazenar o seu robô.
> Não crie ou altere nada fora do diretório de seu projeto

### [3] - Adicione e commit o código de seu robô - [manual](https://docs.gitlab.com/ee/tutorials/make_first_git_commit/#commit-and-push-your-changes)

### [4] - Faça o merge request de suas alterações - [manual](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#when-you-work-in-a-fork)
> Na mensagem do merge request, no campo de comentários, coloque o nome dos participantes de sua equipe.

> Caso exista mais de um commit, faça um Squash durante o merge request.  -- *Squash é uma opção na tela de merge request*


#### Para facilitar sua vida, acesse o [git-cheat-sheet](https://about.gitlab.com/images/press/git-cheat-sheet.pdf) e consulte os comandos mais usados.




